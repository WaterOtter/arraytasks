﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrayTask3 : MonoBehaviour
{
    private int[] ar = new int[10];
    private int max = 0;
    void Start()
    {
        for (int i = 0; i < 10; i++)
        {

            ar[i] = Random.Range(-7, 9); 
            if (ar[i] < max)
            {
                max = ar[i];
            }
        }
        if(max == 0)
        {
            Debug.Log("отрицательных значений нет");
        }
        else
        {
            Debug.Log("маскимальное отрицательное значение: " + max);
        }

    }
    void Update()
    {
        
    }
}
