﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArrayTask4 : MonoBehaviour
{
    private int[] ar = new int[10];
    private int sum;
    public Text mtext;

    void Start()
    {
        for (int i = 0; i < 10; i++)
        {
            // ar[i] = randr.Next(0, 4);
            ar[i] = Random.Range(0, 4);
            mtext.text += (" + " + ar[i].ToString());
            sum = sum + ar[i];
        }
        mtext.text += (" = " + sum.ToString());
    }

    void Update()
    {
        
    }
}
