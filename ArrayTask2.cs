﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ArrayTask2 : MonoBehaviour
{
    public int[] arr = new int[5] { 3, 4, 5, 6, 7 };
    public Text text;

    void Start()
    {
        Array.Reverse(arr);
        for (int i = 0; i < arr.Length; i++)
        {
            text.text += arr[i].ToString();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
